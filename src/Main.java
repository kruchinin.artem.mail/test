import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static List<String> allStrings = ReadFile.readFile("input1");
    private static List<String> firstInput = new ArrayList<>();
    private static List<String> secondInput = new ArrayList<>();
    private static int count = 0;


    public static void main(String[] args) {

        addToList(firstInput);
        addToList(secondInput);

        new WriteFile().writeInFile(FindSimilarWords.findSimilar(firstInput, secondInput));
    }

    private static void addToList(List<String> list) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt() + count;

        for (; count < n; count++) {
            list.add(allStrings.get(count));
        }
    }


}
