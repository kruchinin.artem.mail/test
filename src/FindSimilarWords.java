import java.util.ArrayList;
import java.util.List;

public class FindSimilarWords {
    private static List<String> output = new ArrayList<>();

    public static List<String> findSimilar(List<String> firstInput, List<String> secondInput) {
        for (String item1 : firstInput) {
            boolean similarWasFound = false;
            for (String item2 : secondInput) {

                if (firstInput.size() == 1 && secondInput.size() == 1) {
                    output.add(item1 + ':' + item2);
                    similarWasFound = true;
                }

                if (isSimilar(item1, item2)) {
                    output.add(item1 + ':' + item2);
                    similarWasFound = true;
                }
            }

            if (!similarWasFound)
                output.add(item1 + ":?");
        }
        return output;
    }

    private static boolean isSimilar(String first, String second) {
        String firstLower = first.toLowerCase();
        String secondLower = second.toLowerCase();

        for (String partOfFirst : firstLower.split(" ")) {
            if (secondLower.contains(partOfFirst))
                return true;
        }

        return false;
    }
}
