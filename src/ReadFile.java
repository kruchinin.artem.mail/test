import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadFile {

    public static List<String> readFile(String fileName) {
        File file = new File("src/resources/" + fileName);
        List<String> fileStrings = new ArrayList<>();


        try {
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                fileStrings.add(line);
            }
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileStrings;
    }
}
