import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;

public class WriteFile {

    public void writeInFile (List<String> output) {
        try (PrintWriter pw = new PrintWriter(new FileWriter("src/resources/output"))) {

            for (String result : output) {
                pw.println(result);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
